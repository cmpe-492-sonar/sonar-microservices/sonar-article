const fs = require('fs');
const path = require('path');
const swaggerUiAssetPath = require("swagger-ui-dist").getAbsoluteFSPath()
const copydir = require('copy-dir');
const swagger2openapi = require('swagger2openapi');

const generateSwaggerSpec = async () => {
  const { spec } = require('../api/apiDocs');

  let options = {
    patch: true,
    anchors: true,
  };

  const { openapi } = await swagger2openapi.convertObj(spec, options);

  openapi.components.securitySchemes.Bearer = {
    type: 'http',
    scheme: 'bearer',
    bearerFormat: 'JWT'
  };

  openapi.servers = [
    { url: 'https://sonar-project.cf/api/article/' },
    { url: 'https://article.sonar-project.cf/' },
    { url: '/' },
    { url: '/api/article/' }
  ]

  copydir.sync(swaggerUiAssetPath, path.join(__dirname, '../../docs'));

  fs.writeFileSync(path.join(__dirname, '../../docs/swagger.json'),
    JSON.stringify(openapi, null, '  '),
    { encoding: 'utf8' });

  const html = fs.readFileSync(path.join(__dirname, '../../docs/index.html'),
    { encoding: 'utf8'});
  fs.writeFileSync(path.join(__dirname, '../../docs/index.html'),
    // html.replace(/url:\s*"[^"]+",/, `spec: ${JSON.stringify(openapi)},`), // option A
    html.replace(/url:\s*"[^"]+",/, `url: "swagger.json",`), // option B
    { encoding: 'utf8'})
}


if (typeof module !== 'undefined' && !module.parent) {
  // this is the main module
  generateSwaggerSpec()
}
