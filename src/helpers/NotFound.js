import APIError from "./APIError";

class NotFound extends APIError {
  constructor(message, data) {
    super(message, 404, data);
  }
  static assert(condition, message = "Not Found", data) {
    if (!condition) {
      throw new this(message, data);
    }
  }
}

export default NotFound;