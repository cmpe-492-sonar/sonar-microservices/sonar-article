import Joi from "./Joi";

export const requireUser = (ctx, next) => {
  if (ctx.state.user) {
    return next();
  }
  ctx.unauthorized('Unauthorized');
}

const AUTH_HEADER_REGEX = /^Bearer\s+[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)?$/;
export const authHeader = Joi.object({
  'Authorization': Joi.string().regex(AUTH_HEADER_REGEX)
}).unknown();

export const authHeaderRequired = Joi.object({
  'Authorization': Joi.string().regex(AUTH_HEADER_REGEX).required()
}).unknown();