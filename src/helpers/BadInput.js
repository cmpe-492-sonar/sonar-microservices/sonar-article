import APIError from "./APIError";

class BadInput extends APIError {
  constructor(message, data) {
    super(message, 400, data);
  }
  static assert(condition, message="Bad Input", data) {
    if (!condition) {
      throw new this(message, data);
    }
  }
  // TODO may use joi validate instead
  static mayOnlyContainFields(obj, fields) {
    const fieldSet = new Set(fields);
    const extras = Object.keys(obj)
      .filter((key) => obj.hasOwnProperty(key))
      .filter((key) => !fieldSet.has(key));
    this.assert(extras.length === 0,
      `Bad Input: Fields "${extras.join('", "')}" are given but should not be`, obj);
  }
  static mustContainFields(obj, fields) {
    const fieldSet = new Set(
      Object.keys(obj)
        .filter((key) => obj.hasOwnProperty(key))
    );
    const missing = fields.filter((key) => !fieldSet.has(key));
    this.assert(missing.length === 0,
      `Bad Input: Fields "${missing.join('", "')}" are required but not given`, obj);
  }

}

export default BadInput