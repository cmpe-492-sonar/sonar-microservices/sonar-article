import router from "koa-joi-router"
import JoiObjectId from "joi-mongodb-objectid"

const JoiBase = router.Joi;
JoiObjectId.base = JoiBase.any().meta({
  swagger: {
    type: 'string',
    minLength: 24,
    maxLength: 24,
    pattern: '^[a-f0-9]*$',
    format: 'hex',
    required: true,
  },
  swaggerOverride: true,
});
const Joi = JoiBase.extend(JoiObjectId);

Joi.mongoObject = Joi.object({
  _id: Joi.objectId().required(),
  __v: Joi.number(),
  createdAt: Joi.date(),
  updatedAt: Joi.date(),
})

Joi.doi = () => Joi.string().lowercase();

export default Joi;