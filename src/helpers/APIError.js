class APIError extends Error {
  constructor(message, status, data) {
    super(message);
    if (status) {
      this.status = status;
    }
    if (data) {
      this.data = data;
    }
  }
  static assert(condition, message = "Assertion Failed", status, data) {
    if (!condition) {
      throw new this(message, status, data);
    }
  }
}

export default APIError;