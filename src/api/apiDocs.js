import {SwaggerAPI} from "koa-joi-router-docs";
import articleRouter from "./article";
import articleListRouter from "./articleList";

const generator = new SwaggerAPI()

generator.addJoiRouter(articleRouter, { prefix: '/article' });
generator.addJoiRouter(articleListRouter, { prefix: '/articleList' });

export const spec = generator.generateSpec({
  info: {
    title: 'Sonar Article',
    description: 'Sonar Article microservice.',
    version: '1.0.0'
  },
  basePath: '/',
  securityDefinitions: {
    Bearer: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header',
      template: "Bearer {apiKey}"
    }
  },
  security: [
    {
      Bearer: []
    }
  ]
})

export const html = `
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Example API</title>
  </head>
  <body>
    <redoc spec-url='_api.json' lazy-rendering></redoc>
    <script src="https://rebilly.github.io/ReDoc/releases/latest/redoc.min.js"></script>
  </body>
  </html>
  `;

