import getMongooseInstance from 'connect-mongoose-lambda/getMongooseInstance';

const mongoose = getMongooseInstance({ instance: 'article' });

const ArticleListSchema = new mongoose.Schema({
  authorId: { type: String, required: true },
  authorName: { type: String, required: true },
  title: { type: String, required: true },
  articles: [{ type: 'ObjectId', ref: 'Article' }],
  public: { type: Boolean, default: false },
}, {
  timestamps: true
});

ArticleListSchema.statics.updatableFields = ['title', 'articles', 'public'];
ArticleListSchema.statics.requiredFields = ['title', 'articles'];

ArticleListSchema.index({ authorId: 1, public: 1 })

const ArticleListModel = mongoose.models.ArticleList ||
  mongoose.model('ArticleList', ArticleListSchema);

export default ArticleListModel;
