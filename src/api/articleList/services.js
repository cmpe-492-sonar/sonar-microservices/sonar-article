import ArticleListModel from "./model";
import * as ArticleServices from "../article/services";
import {Types} from 'mongoose';
import BadInput from "../../helpers/BadInput";

export const getLatestPublicArticleLists = () => {
  return ArticleListModel
    .find({
      public: true
    }).sort({ updatedAt: -1 }).limit(50).lean()
}

export const getArticleListsByUser = (userId, publicOnly) => {
  const query = { authorId: userId };
  if (publicOnly) {
    query.public = true;
  }
  return ArticleListModel
    .find(query)
    .lean();
}

export const getArticleListById = (articleListId, userId) => {
  return ArticleListModel
    .findOne({
      _id: articleListId,
      $or: [
        { public: true },
        { authorId: userId }
      ]
    })
    .lean()
    .populate('articles');
}

export const updateArticleListById = async (articleListId, payload, userId) => {
  // BadInput.mayOnlyContainFields(payload, ArticleListModel.updatableFields);
  const { articles, ...data } = payload;
  let articleList = await ArticleListModel.findOne({
    _id: new Types.ObjectId(articleListId),
    authorId: userId
  });
  if (articles) {
    const finalArticles = await ArticleServices.addArticlesByDoiList(articles)
    data.articles = finalArticles.map(({ _id }) => _id);
  }
  Object.assign(articleList, data);
  articleList = await articleList.save();
  return articleList.toObject();
}

export const deleteArticleListById = async (articleListId, userId) => {
  const result = await ArticleListModel.deleteOne({
    _id: new Types.ObjectId(articleListId),
    authorId: userId
  });
  return result;
}

export const createArticleList = async (payload, userId) => {
  // BadInput.mustContainFields(payload, ArticleListModel.requiredFields);
  // BadInput.mayOnlyContainFields(payload, ArticleListModel.updatableFields);
  const { articles, ...data } = payload
  const finalArticles = await ArticleServices.addArticlesByDoiList(articles);
  data.articles = finalArticles.map(({ _id }) => _id);
  data.authorId = userId;
  let articleList = new ArticleListModel(data);
  articleList = await articleList.save();
  return articleList.toObject();
}
