import router from 'koa-joi-router';
import Joi from "../../helpers/Joi";
import {requireUser} from "../../helpers/auth";
import * as Services from "./services";
import * as Validators from "./validators";

const articleListRouter = router();

articleListRouter.route({
  meta: {
    swagger: {
      summary: 'Get Latest Article Lists',
    }
  },
  method: 'get',
  path: '/latest',
  validate: {
    output: {
      200: {
        body: Joi.array()
          .items(Validators.articleList).required()
      }
    }
  },
  pre: requireUser,
  handler: async (ctx) => {
    const result = await Services
      .getLatestPublicArticleLists();
    ctx.ok(result);
  },
})

articleListRouter.route({
  meta: {
    swagger: {
      summary: 'Get My Article Lists',
    }
  },
  method: 'get',
  path: '/',
  validate: {
    output: {
      200: {
        body: Joi.array()
          .items(Validators.articleList).required()
      }
    }
  },
  pre: requireUser,
  handler: async (ctx) => {
    const result = await Services
      .getArticleListsByUser(ctx.state.user.sub);
    ctx.ok(result);
  },
})

articleListRouter.route({
  meta: {
    swagger: {
      summary: 'Get Article Lists of User',
    }
  },
  method: 'get',
  path: '/user/:userId',
  validate: {
    params: {
      userId: Joi.string().required()
    },
    output: {
      200: {
        body: Joi.array()
          .items(Validators.articleList).required()
      }
    }
  },
  handler: async (ctx) => {
    const isOwner = (ctx.state.user || {}).sub === ctx.params.userId;
    const result = await Services
      .getArticleListsByUser(ctx.params.userId, !isOwner);
    ctx.ok(result);
  },
})

articleListRouter.route({
  meta: {
    swagger: {
      summary: 'Get Article List By Id',
    }
  },
  method: 'get',
  path: '/:id',
  validate: {
    params: {
      id: Joi.objectId().required()
    },
    output: {
      200: {
        body: Validators.articleListPopulated.required()
      }
    }
  },
  handler: async (ctx) => {
    const result = await Services
      .getArticleListById(ctx.params.id, (ctx.state.user || {}).sub);
    if (!result) {
      return ctx.notFound('Article List not found or you cannot view it');
    }
    ctx.ok(result);
  },
});

articleListRouter.route({
  meta: {
    swagger: {
      summary: 'Create Article List',
    }
  },
  method: 'post',
  path: '/',
  validate: {
    body: Validators.articleListCreate.required(),
    type: 'json',
    output: {
      201: {
        body: Validators.articleList.required()
      }
    }
  },
  pre: requireUser,
  handler: async (ctx) => {
    const result = await Services
      .createArticleList(ctx.request.body, ctx.state.user.sub);
    ctx.created(result);
  }
})

articleListRouter.route({
  meta: {
    swagger: {
      summary: 'Update Article List',
    }
  },
  method: 'put',
  path: '/:articleListId',
  validate: {
    params: {
      articleListId: Joi.objectId().required()
    },
    body: Validators.articleListUpdate.required(),
    type: 'json',
    output: {
      200: {
        body: Validators.articleList.required()
      }
    }
  },
  pre: requireUser,
  handler: async (ctx) => {
    const result = await Services
      .updateArticleListById(ctx.params.articleListId, ctx.request.body, ctx.state.user.sub);
    if (!result) {
      return ctx.notFound('Article List not found or you cannot update it');
    }
    ctx.ok(result);
  }
})

articleListRouter.route({
  meta: {
    swagger: {
      summary: 'Delete Article List',
    }
  },
  method: 'delete',
  path: '/:articleListId',
  validate: {
    params: {
      articleListId: Joi.objectId().required()
    },
    body: Validators.articleListUpdate.required(),
    type: 'json',
    output: {
      200: {
        body: Validators.articleList.required()
      }
    }
  },
  pre: requireUser,
  handler: async (ctx) => {
    const result = await Services
      .deleteArticleListById(ctx.params.articleListId, ctx.state.user.sub);
    ctx.ok();
  }
})

export default articleListRouter;
