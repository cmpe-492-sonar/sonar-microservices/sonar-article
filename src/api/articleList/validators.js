import Joi from "../../helpers/Joi";
import * as ArticleValidators from "../article/validators";

export const articleListUpdate = Joi.object({
  title: Joi.string(),
  public: Joi.boolean(),
  articles: Joi.array().items(Joi.doi()),
  authorName: Joi.string(),
})

export const articleListCreate = articleListUpdate.keys({
  title: Joi.string().required(),
  authorName: Joi.string().required(),
  articles: Joi.array().items(Joi.doi()).default([]),
})

export const articleList = Joi.mongoObject.keys({
  title: Joi.string().required(),
  authorId: Joi.string(),
  authorName: Joi.string(),
  public: Joi.boolean().required(),
  articles: Joi.array().items(Joi.objectId()).required(),
})

export const articleListPopulated = articleList.keys({
  articles: Joi.array().items(ArticleValidators.article).required()
})
