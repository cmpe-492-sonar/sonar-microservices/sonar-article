import Router from '@koa/router';
import articleRouter from "./article";
import articleListRouter from "./articleList";
import * as ApiDocs from "./apiDocs";

const apiRouter = new Router();

apiRouter.use('/article', articleRouter.middleware());

apiRouter.use('/articleList', articleListRouter.middleware());

/*apiRouter.get('/_api.json', async (ctx) => {
  ctx.body = JSON.stringify(ApiDocs.spec, null, '  ')
});

apiRouter.get('/apiDocs', (ctx) => {
  ctx.body = ApiDocs.html;
})*/

export default apiRouter;