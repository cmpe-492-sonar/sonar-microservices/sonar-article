import {send} from 'request-and-forget';
import ArticleModel from "./model";

export const getArticleById = (articleId) => {
  return ArticleModel.findById(articleId).lean();
}

export const getArticleByDoi = (doi) => {
  return ArticleModel.findOne({ doi }).lean();
}

export const addArticlesByDoiList = async (doiList) => {
  if (doiList.length === 0) {
    return [];
  }
  const result = await ArticleModel.find({ doi: { $in: doiList }}).lean();
  const existingDoiSet = new Set(result.map(({ doi }) => doi));
  const newDoiList = doiList.filter((doi) => !existingDoiSet.has(doi));
  if (newDoiList.length > 0) {
    const newOnes = await ArticleModel.insertMany(newDoiList.map(doi => ({ doi })));
    result.push(...newOnes);
    // console.log(result, newOnes, newDoiList);

    // trigger sync (start processing of new articles)
    await send({
      url: process.env.SYNC_URL,
      method: 'POST',
      secret_token: process.env.SYNC_SECRET
    })
  }

  return result;
}

