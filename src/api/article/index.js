const router = require('koa-joi-router');
import Joi from "../../helpers/Joi";

import * as Services from "./services";
import * as Validators from "./validators";

const articleRouter = router();

articleRouter.route({
  meta: {
    swagger: {
      summary: 'Get Article by DOI',
    }
  },
  method: 'get',
  path: '/',
  validate: {
    query: {
      doi: Joi.doi().required()
    },
    output: {
      200: {
        body: Validators.article.required()
      }
    }
  },
  handler: async (ctx) => {
    const { doi } = ctx.query;
    const result = await Services.getArticleByDoi(doi);
    ctx.ok(result);
  }
});


articleRouter.route({
  meta: {
    swagger: {
      summary: 'Get Article by ArticleId',
    }
  },
  method: 'get',
  path: '/:articleId',
  validate: {
    params: {
      articleId: Joi.objectId().required()
    },
    output: {
      200: {
        body: Validators.article.required()
      }
    }
  },
  handler: async (ctx) => {
    const { articleId } = ctx.params;
    const result = await Services.getArticleById(articleId);
    ctx.ok(result);
  }
})


export default articleRouter;