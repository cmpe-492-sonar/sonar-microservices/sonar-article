import Joi from "../../helpers/Joi";

export const article = Joi.mongoObject.keys({
  doi: Joi.doi().required(),
  status: Joi.number().required(),
  crawler: Joi.number().required(),
  error: Joi.any()
});