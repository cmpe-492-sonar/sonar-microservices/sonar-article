import getMongooseInstance from 'connect-mongoose-lambda/getMongooseInstance';

const mongoose = getMongooseInstance({ instance: 'article' });

const ArticleSchema = new mongoose.Schema({
  doi: { type: String, unique: true },
  status: { type: Number, default: 0, index: true },
  // 0 -> waiting, 1 -> copying to crawler db, 2 -> copied/processing, 3 -> success, -1 -> unknown error
  crawler: { type: Number, default: 0 },
  // 0 -> elsevier, 1 -> ieee, last -> not found.
  error: { type: mongoose.Schema.Types.Mixed }
}, {
  timestamps: true
});

ArticleSchema.index({ doi: 1, crawler: 1})

const ArticleModel = mongoose.models.Article ||
  mongoose.model('Article', ArticleSchema);

export default ArticleModel;