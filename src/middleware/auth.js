import { koaJwtSecret } from 'jwks-rsa'
import jwt from 'koa-jwt'

const authMiddleware = jwt({
  secret: koaJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: process.env.AUTH_JWKS_URI
  }),

  // Validate the audience and the issuer
  audience: process.env.AUTH_AUDIENCE,
  issuer: process.env.AUTH_ISSUER,
  algorithms: [ 'RS256' ],

  // ctx.state.user is set if auth is given, not set otherwise
  passthrough: true
})


export default authMiddleware;