import path from 'path';
import Koa from 'koa';
import helmet from 'koa-helmet';
import logger from 'koa-logger';
import respond from 'koa-respond';
import bodyParser from 'koa-bodyparser';
import connectMongoDB from 'connect-mongoose-lambda';
import staticServer from 'koa-static-server';

import apiRouter from "./api";
import authMiddleware from "./middleware/auth";

const app = new Koa();

app.use(helmet());

app.use(logger());

app.use(respond());

app.use(bodyParser());

// ERROR HANDLER MIDDLEWARE
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    console.log('ERROR', e);
    let { status, message } = e;
    status = status || 500;
    let body = {
      status: status || 500,
      error: message
    };
    if (e.name === 'StatusCodeError') {
      body.error = `${e.name} - ${e.statusCode}`;
    }
    if (e.data || e.error) {
      body.data = e.error;
    }
    ctx.body = body;
    ctx.status = status || 500;
  }
});

app.use(authMiddleware);

app.use(apiRouter.routes());

app.use(apiRouter.allowedMethods());

app.use(staticServer({
  rootDir: path.join(__dirname, '../docs'),
  rootPath: '/docs/',
  notFoundFile: './index.html',
  log: false,
}))

app.use(async ctx => {
  ctx.notFound('Not Found');
});



connectMongoDB(process.env.ARTICLE_MONGODB_URI, {
  poolSize: process.env.ARTICLE_MONGO_POOL_SIZE || 5
}, {
  instance: 'article'
});

export default app;
