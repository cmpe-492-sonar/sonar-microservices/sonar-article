/* eslint no-process-env: 0, no-extend-native: 0 */
import app from './app';

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Sonar-Article started listening on port ${PORT}`)
});
